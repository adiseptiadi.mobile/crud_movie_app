# movie_app

CRUD Movie App to complete the assessment test as a mobile apps developer (flutter) at CT Corp Digital and Vacancy.

Made by Adi Septiadi

Used Libraries:

- auto_route
- mobx

Mail: adiseptiadi.mobile@gmail.com

Phone: +62821-1452-1101