// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i1;
import 'package:flutter/material.dart' as _i2;

import '../main.dart' as _i3;
import '../model/movie.dart' as _i6;
import '../ui/activity_add_movie.dart' as _i5;
import '../ui/activity_detail_movie.dart' as _i4;

class AppRouter extends _i1.RootStackRouter {
  AppRouter([_i2.GlobalKey<_i2.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i1.PageFactory> pagesMap = {
    HomeRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args =
              data.argsAs<HomeRouteArgs>(orElse: () => const HomeRouteArgs());
          return _i3.HomePage(movie: args.movie);
        }),
    ActivityMovieDetailRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<ActivityMovieDetailRouteArgs>();
          return _i4.ActivityMovieDetailPage(
              movie: args.movie,
              onMovieUpdated: args.onMovieUpdated,
              onMovieRemoved: args.onMovieRemoved);
        }),
    ActivityMovieAddRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<ActivityMovieAddRouteArgs>();
          return _i5.ActivityMovieAddPage(onMovieChanged: args.onMovieChanged);
        })
  };

  @override
  List<_i1.RouteConfig> get routes => [
        _i1.RouteConfig(HomeRoute.name, path: '/'),
        _i1.RouteConfig(ActivityMovieDetailRoute.name,
            path: '/activity-movie-detail-page'),
        _i1.RouteConfig(ActivityMovieAddRoute.name, path: '/addMovie')
      ];
}

class HomeRoute extends _i1.PageRouteInfo<HomeRouteArgs> {
  HomeRoute({List<_i6.Movie>? movie})
      : super(name, path: '/', args: HomeRouteArgs(movie: movie));

  static const String name = 'HomeRoute';
}

class HomeRouteArgs {
  const HomeRouteArgs({this.movie});

  final List<_i6.Movie>? movie;
}

class ActivityMovieDetailRoute
    extends _i1.PageRouteInfo<ActivityMovieDetailRouteArgs> {
  ActivityMovieDetailRoute(
      {required _i6.Movie movie,
      required dynamic Function(_i6.Movie) onMovieUpdated,
      required dynamic Function(_i6.Movie) onMovieRemoved})
      : super(name,
            path: '/activity-movie-detail-page',
            args: ActivityMovieDetailRouteArgs(
                movie: movie,
                onMovieUpdated: onMovieUpdated,
                onMovieRemoved: onMovieRemoved));

  static const String name = 'ActivityMovieDetailRoute';
}

class ActivityMovieDetailRouteArgs {
  const ActivityMovieDetailRouteArgs(
      {required this.movie,
      required this.onMovieUpdated,
      required this.onMovieRemoved});

  final _i6.Movie movie;

  final dynamic Function(_i6.Movie) onMovieUpdated;

  final dynamic Function(_i6.Movie) onMovieRemoved;
}

class ActivityMovieAddRoute
    extends _i1.PageRouteInfo<ActivityMovieAddRouteArgs> {
  ActivityMovieAddRoute({required void Function(_i6.Movie) onMovieChanged})
      : super(name,
            path: '/addMovie',
            args: ActivityMovieAddRouteArgs(onMovieChanged: onMovieChanged));

  static const String name = 'ActivityMovieAddRoute';
}

class ActivityMovieAddRouteArgs {
  const ActivityMovieAddRouteArgs({required this.onMovieChanged});

  final void Function(_i6.Movie) onMovieChanged;
}
