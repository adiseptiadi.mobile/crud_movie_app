
import 'package:auto_route/annotations.dart';
import 'package:movie_app/main.dart';
import 'package:movie_app/ui/activity_add_movie.dart';
import 'package:movie_app/ui/activity_detail_movie.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(page: HomePage, initial: true,),
    AutoRoute(page: ActivityMovieDetailPage),
    AutoRoute(path: "/addMovie",page: ActivityMovieAddPage),
  ]
)
class $AppRouter {}