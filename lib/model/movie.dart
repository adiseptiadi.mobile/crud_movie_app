import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:movie_app/router/router.gr.dart';

class Movie{
  int? id;
  String? title;
  String? director;
  String? summary;
  List<String>? tags;

  Movie({this.id, this.title, this.director, this.summary, this.tags});
}

class MovieCard extends StatelessWidget {
  const MovieCard({Key? key, required this.movie, required this.onTapItem}) : super(key: key);

  final VoidCallback onTapItem;
  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: InkWell(
          onTap: (){
            onTapItem();
            // print("MoviesClicked");
            // AutoRouter.of(context).push(ActivityMovieDetailRoute(movie: movie));
          },
          splashColor: Colors.blue.withAlpha(30),
          child: Padding(
            padding: EdgeInsets.only(top: 20, bottom: 20, right: 20, left: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Observer(builder: (context) => Text(movie.title.toString())),
              Observer(builder: (context) => Text(movie.director.toString())),
            ],
          ),
          )
        ),
      ),
    );
  }
}