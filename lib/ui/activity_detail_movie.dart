import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:movie_app/mobx/movie.dart';
import 'package:movie_app/model/movie.dart';
import 'package:movie_app/model/tag.dart';

class ActivityMovieDetailPage extends StatefulWidget {
  final Movie movie;
  final Function(Movie) onMovieUpdated;
  final Function(Movie) onMovieRemoved;

  const ActivityMovieDetailPage({required this.movie, required this.onMovieUpdated, required this.onMovieRemoved});

  @override
  _ActivityMovieDetailPage createState() => _ActivityMovieDetailPage();
}

class _ActivityMovieDetailPage extends State<ActivityMovieDetailPage>{
  var _defaultTag = null;

  String title = "Update Movie";
  List<String> _tagsStr = ["Action", "Comedy", "Fantasy", "Horror", "Sci-Fi"];
  // List<String> _selectedTags = [];

  late Movie movie;
  final MovieMobx movieList = MovieMobx();
  final MovieMobx movieTags = MovieMobx();

  final _multiSelectKey = GlobalKey<FormFieldState>();

  TextEditingController titleCtr = TextEditingController();
  TextEditingController directorCtr = TextEditingController();
  TextEditingController summaryCtr = TextEditingController();

  Movie getMovies() {
    return movie;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    titleCtr.text = widget.movie.title.toString();
    directorCtr.text = widget.movie.director.toString();
    summaryCtr.text = widget.movie.summary.toString();

    print("DetailMovie::title:: "+widget.movie.title.toString());
    print("DetailMovie::tag:: "+widget.movie.tags.toString());
    
    movie = widget.movie;

    widget.movie.tags!.forEach((item) => movieTags.tags.add(item));
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(this.title),
        actions: [
          FlatButton(
            textColor: Colors.white,
            onPressed: () {
              widget.onMovieRemoved(getMovies());
              context.router.pop();
            },
            child: Text("Delete"),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
          FlatButton(
            textColor: Colors.white,
            onPressed: () {
              print("update::movie::new_tag:: "+movieTags.tags.toString());
              Movie mv = new Movie();
              mv.id = widget.movie.id;
              mv.title = titleCtr.text;
              mv.director = directorCtr.text;
              mv.summary = summaryCtr.text;
              mv.tags = movieTags.tags;
              // movieList.update(mv);
              movie = mv;
              
              widget.onMovieUpdated(getMovies());
              context.router.pop();
            },
            child: Text("Update"),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                  child: TextField(
                    autocorrect: false,
                    autofocus: true,
                    showCursor: true,
                    controller: titleCtr,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: "Title",
                      labelStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.normal),
                      hintStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.normal),
                      enabledBorder: const OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1.0),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(
                            color: Color.fromRGBO(235, 162, 16, 1), width: 1.0),
                      ),
                    ),
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.normal),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                  child: TextField(
                    autocorrect: false,
                    autofocus: true,
                    showCursor: true,
                    controller: directorCtr,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: "Director",
                      labelStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.normal),
                      hintStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.normal),
                      enabledBorder: const OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1.0),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(
                            color: Color.fromRGBO(235, 162, 16, 1), width: 1.0),
                      ),
                    ),
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.normal),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                  child: TextField(
                    autocorrect: false,
                    autofocus: true,
                    showCursor: true,
                    controller: summaryCtr,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: "Summary",
                      labelStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.normal),
                      hintStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.normal),
                      enabledBorder: const OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1.0),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(
                            color: Color.fromRGBO(235, 162, 16, 1), width: 1.0),
                      ),
                    ),
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.normal),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                  child: InkWell(
                      onTap: () {
                        print("masok");
                      },
                      child: Container(
                          padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                          decoration: BoxDecoration(
                              border:
                                  Border.all(width: 1, color: Colors.black)),
                          child: Column(
                            children: [
                              Observer(builder: (_) {
                                return Column(
                                  children: [
                                    DropdownButtonFormField<String>(
                                        value: _defaultTag,
                                        decoration: InputDecoration(
                                            fillColor: Colors.transparent,
                                            labelText: "Tags"),
                                        items: _tagsStr.map((String val) {
                                          return DropdownMenuItem<String>(
                                            value: val,
                                            child: new Text(val),
                                          );
                                        }).toList(),
                                        // hint: Text("Tags"),
                                        onChanged: (newVal) {
                                          setState(() {
                                            _defaultTag = null;
                                          });
                                          movieTags.addTag(newVal.toString());
                                        }),
                                    movieTags.tags.length == 0
                                        ? Container(
                                            child: Text("Choose Tags"),
                                          )
                                        : Container(
                                            child: GridView.count(
                                            shrinkWrap: true,
                                            childAspectRatio: (1 / .3),
                                            physics: ScrollPhysics(),
                                            crossAxisCount: 2,
                                            children: List.generate(
                                                movieTags.tags.length, (index) {
                                              return Container(
                                                  margin: EdgeInsets.all(5),
                                                  padding: EdgeInsets.fromLTRB(
                                                      10, 2, 0, 2),
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          width: 1,
                                                          color: Colors.black)),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: [
                                                      Expanded(
                                                        child: Text(movieTags.tags
                                                            [index]
                                                            .toString()),
                                                      ),
                                                      Expanded(
                                                          child: Align(
                                                        alignment: Alignment
                                                            .centerRight,
                                                        child: IconButton(
                                                          icon: Icon(
                                                              Icons.cancel,
                                                              size: 16),
                                                          onPressed: () {
                                                            movieTags.deleteTag(movieTags.tags[index]);
                                                          },
                                                        ),
                                                      ))
                                                    ],
                                                  ));
                                            }),
                                          )),
                                  ],
                                );
                              }),
                            ],
                          ))),
                )
              ],
            )),
      ),
    );
  }
}