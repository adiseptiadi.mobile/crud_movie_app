import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:movie_app/mobx/movie.dart';
import 'package:movie_app/model/movie.dart';
import 'package:movie_app/model/tag.dart';
import 'package:movie_app/router/router.gr.dart';

class ActivityMovieAddPage extends StatefulWidget {
  final void Function(Movie) onMovieChanged;

  const ActivityMovieAddPage(
      {required this.onMovieChanged});

  @override
  _ActivityMovieAddState createState() => _ActivityMovieAddState();
}

class _ActivityMovieAddState extends State<ActivityMovieAddPage> {
  var _defaultTag = null;

  String title = "New Movie";
  List<String> _tagsStr = ["Action", "Comedy", "Fantasy", "Horror", "Sci-Fi"];
  List<String> _selectedTags = [];

  late Movie movie;
  final MovieMobx movieList = MovieMobx();
  final MovieMobx movieTag = MovieMobx();

  final _multiSelectKey = GlobalKey<FormFieldState>();

  TextEditingController titleCtr = TextEditingController();
  TextEditingController directorCtr = TextEditingController();
  TextEditingController summaryCtr = TextEditingController();

  Movie getMovies() {
    // return movieList.movies;
    return movie;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    context.router.pop("Mantul gan");

    titleCtr.dispose();
    directorCtr.dispose();
    summaryCtr.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.title),
        actions: [
          FlatButton(
            textColor: Colors.white,
            onPressed: () {
              Movie mv = new Movie();
              mv.title = titleCtr.text;
              mv.director = directorCtr.text;
              mv.summary = summaryCtr.text;
              mv.tags = movieTag.tags;
              // movieList.add(mv);
              movie = mv;
              // print("Movie::Length::After:: " +
              //     movieList.movies.length.toString());
              widget.onMovieChanged(getMovies());
              context.router.pop();
            },
            child: Text("Save"),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                  child: TextField(
                    autocorrect: false,
                    autofocus: true,
                    showCursor: true,
                    controller: titleCtr,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: "Title",
                      labelStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.normal),
                      hintStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.normal),
                      enabledBorder: const OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1.0),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(
                            color: Color.fromRGBO(235, 162, 16, 1), width: 1.0),
                      ),
                    ),
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.normal),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                  child: TextField(
                    autocorrect: false,
                    autofocus: true,
                    showCursor: true,
                    controller: directorCtr,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: "Director",
                      labelStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.normal),
                      hintStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.normal),
                      enabledBorder: const OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1.0),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(
                            color: Color.fromRGBO(235, 162, 16, 1), width: 1.0),
                      ),
                    ),
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.normal),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                  child: TextField(
                    autocorrect: false,
                    autofocus: true,
                    showCursor: true,
                    controller: summaryCtr,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: "Summary",
                      labelStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.normal),
                      hintStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.normal),
                      enabledBorder: const OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1.0),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(
                            color: Color.fromRGBO(235, 162, 16, 1), width: 1.0),
                      ),
                    ),
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.normal),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                  child: InkWell(
                      onTap: () {
                        print("masok");
                      },
                      child: Container(
                          padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                          decoration: BoxDecoration(
                              border:
                                  Border.all(width: 1, color: Colors.black)),
                          child: Column(
                            children: [
                              Observer(builder: (_) {
                                return Column(
                                  children: [
                                    DropdownButtonFormField<String>(
                                        value: _defaultTag,
                                        decoration: InputDecoration(
                                            fillColor: Colors.transparent,
                                            labelText: "Tags"),
                                        items: _tagsStr.map((String val) {
                                          return DropdownMenuItem<String>(
                                            value: val,
                                            child: new Text(val),
                                          );
                                        }).toList(),
                                        // hint: Text("Tags"),
                                        onChanged: (newVal) {
                                          setState(() {
                                            _defaultTag = null;
                                            // _selectedTags
                                            //     .add(newVal.toString());
                                          });
                                          movieTag.addTag(newVal.toString());
                                        }),
                                    movieTag.tags.length == 0
                                        ? Container(
                                            child: Text("Choose Tags"),
                                          )
                                        : Container(
                                            child: GridView.count(
                                            shrinkWrap: true,
                                            childAspectRatio: (1 / .3),
                                            physics: ScrollPhysics(),
                                            crossAxisCount: 2,
                                            children: List.generate(
                                                movieTag.tags.length, (index) {
                                              return Container(
                                                  margin: EdgeInsets.all(5),
                                                  padding: EdgeInsets.fromLTRB(
                                                      10, 2, 0, 2),
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          width: 1,
                                                          color: Colors.black)),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: [
                                                      Expanded(
                                                        child: Text(
                                                            movieTag.tags[index]
                                                                .toString()),
                                                      ),
                                                      Expanded(
                                                          child: Align(
                                                        alignment: Alignment
                                                            .centerRight,
                                                        child: IconButton(
                                                          icon: Icon(
                                                              Icons.cancel,
                                                              size: 16),
                                                          onPressed: () {
                                                            // _selectedTags
                                                            //     .removeAt(
                                                            //         index);
                                                            movieTag.deleteTag(movieTag.tags[index]);
                                                          },
                                                        ),
                                                      ))
                                                    ],
                                                  ));
                                            }),
                                          )),
                                  ],
                                );
                              }),
                            ],
                          ))),
                )
              ],
            )),
      ),
    );
  }
}
