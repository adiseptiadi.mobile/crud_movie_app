import 'package:mobx/mobx.dart';
import 'package:movie_app/model/movie.dart';

part 'movie.g.dart';

class MovieMobx = _MovieMobx with _$MovieMobx;

abstract class _MovieMobx with Store{

  @observable
  ObservableList<Movie> movies = ObservableList();

  @observable
  ObservableList<String> tags = ObservableList();

  @action
  void add(Movie mv){
    // print("addMovie:: "+mv.toString());
    // int lastId = 0;
    // if ( movies.length > 0 ){
    //   for (int x=0; x < movies.length; x++){
    //     lastId = movies[x].id!;
    //   }
    // }

    // lastId++;

    // mv.id = lastId;

    print("addMovie:: success :: lastId:: "+mv.id.toString());

    movies.add(mv);
  }

  @action
  void update(Movie mv){
    movies[movies.indexWhere((item) => item.id == mv.id)] = mv;
  }

  @action
  void delete(int id){
    movies.removeWhere((item) => item.id == id);
  }

  @action
  void addTag(String selectedTags){
    var tag = tags.where((element) => element == selectedTags);
    if (tag.isEmpty){
      tags.add(selectedTags);
    } 
  }

  @action
  void updateTag(String selectedTags){
    tags[tags.indexWhere((item) => item == selectedTags)] = selectedTags;
  }

  @action
  void deleteTag(String selectedTags){
    tags.removeWhere((item) => item == selectedTags);
  }

}