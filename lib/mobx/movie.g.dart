// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MovieMobx on _MovieMobx, Store {
  final _$moviesAtom = Atom(name: '_MovieMobx.movies');

  @override
  ObservableList<Movie> get movies {
    _$moviesAtom.reportRead();
    return super.movies;
  }

  @override
  set movies(ObservableList<Movie> value) {
    _$moviesAtom.reportWrite(value, super.movies, () {
      super.movies = value;
    });
  }

  final _$tagsAtom = Atom(name: '_MovieMobx.tags');

  @override
  ObservableList<String> get tags {
    _$tagsAtom.reportRead();
    return super.tags;
  }

  @override
  set tags(ObservableList<String> value) {
    _$tagsAtom.reportWrite(value, super.tags, () {
      super.tags = value;
    });
  }

  final _$_MovieMobxActionController = ActionController(name: '_MovieMobx');

  @override
  void add(Movie mv) {
    final _$actionInfo =
        _$_MovieMobxActionController.startAction(name: '_MovieMobx.add');
    try {
      return super.add(mv);
    } finally {
      _$_MovieMobxActionController.endAction(_$actionInfo);
    }
  }

  @override
  void update(Movie mv) {
    final _$actionInfo =
        _$_MovieMobxActionController.startAction(name: '_MovieMobx.update');
    try {
      return super.update(mv);
    } finally {
      _$_MovieMobxActionController.endAction(_$actionInfo);
    }
  }

  @override
  void delete(int id) {
    final _$actionInfo =
        _$_MovieMobxActionController.startAction(name: '_MovieMobx.delete');
    try {
      return super.delete(id);
    } finally {
      _$_MovieMobxActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addTag(String selectedTags) {
    final _$actionInfo =
        _$_MovieMobxActionController.startAction(name: '_MovieMobx.addTag');
    try {
      return super.addTag(selectedTags);
    } finally {
      _$_MovieMobxActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateTag(String selectedTags) {
    final _$actionInfo =
        _$_MovieMobxActionController.startAction(name: '_MovieMobx.updateTag');
    try {
      return super.updateTag(selectedTags);
    } finally {
      _$_MovieMobxActionController.endAction(_$actionInfo);
    }
  }

  @override
  void deleteTag(String selectedTags) {
    final _$actionInfo =
        _$_MovieMobxActionController.startAction(name: '_MovieMobx.deleteTag');
    try {
      return super.deleteTag(selectedTags);
    } finally {
      _$_MovieMobxActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
movies: ${movies},
tags: ${tags}
    ''';
  }
}
