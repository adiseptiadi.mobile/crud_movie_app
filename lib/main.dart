import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:movie_app/ui/activity_add_movie.dart';
import 'router/router.gr.dart';
import 'package:movie_app/mobx/movie.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'model/movie.dart';

final MovieMobx movies = MovieMobx();
final _appRouter = AppRouter();

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // return MaterialApp(
    //   title: 'Movie App',
    //   theme: ThemeData(
    //     primarySwatch: Colors.blue,
    //   ),
    //   home: HomePage(title: 'Movies'),
    // );
    return MaterialApp.router(
      routerDelegate: _appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),
    );
  }
}

class HomePage extends StatefulWidget {
  final List<Movie>? movie;

  HomePage({this.movie});

  String title = "Movies";

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    print("HomePage::MovieLength:: " + movies.movies.length.toString());
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          FlatButton(
            textColor: Colors.white,
            onPressed: () {
              context.router.navigate(ActivityMovieAddRoute(
                  // movie: movies.movies,
                  onMovieChanged: (v) {
                    int size = movies.movies.length;
                    int lastId = 0;
                    
                    for(int i=0; i < size; i++){
                      lastId = movies.movies[i].id!;
                    }
                    lastId++;
                    v.id = lastId;

                    movies.movies.add(v);
                // v.forEach((item) => movies.movies.add(item));
              }));
            },
            child: Text("New"),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
        ],
      ),
      body: Container(
        child: Observer(builder: (_) {
          return ListView(
            scrollDirection: Axis.vertical,
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            children: List.generate(movies.movies.length, (index) {
              return MovieCard(
                movie: movies.movies[index],
                onTapItem: () {
                  print("MoviesClicked::id::"+movies.movies[index].id.toString());
                  context.router.navigate(ActivityMovieDetailRoute(
                      movie: movies.movies[index],
                      onMovieUpdated: (v) {
                        print("update::movie::home:: "+v.tags.toString());
                        Movie mv = v;
                        movies.update(v);
                      },
                      onMovieRemoved: (v) {
                        Movie mv = v;
                        int? id = mv.id;

                        print("delete::movie::id:: " + id.toString());

                        movies.delete(id!);
                      },
                      ));
                  // AutoRouter.of(context)
                  //     .push(ActivityMovieDetailRoute(movie: movies.movies[index]));
                },
              );
            }),
          );
        }),
      ),
    );
  }
}
